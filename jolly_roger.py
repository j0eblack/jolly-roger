#!/usr/bin/env python3

import curses
import time
import shutil
import requests
import json
import os
from bs4 import BeautifulSoup
from pick import pick
from subprocess import call,Popen

banner = """                          _`                                              
                          c!                                              
                         .Ey`                                             
                        ,P@&,            `                                
                      `!Q@@B*          ,*~                                
                     ~A@@K!;Y`      `;L!`                                 
                    `m@@m` `z,    ,vgQ!                                   
                    ,W@@i   *+ ';|N@@N'         '!.                       
                 `~}Q@@X'   ;m!!cN@@@@c`      ~xi,                        
          ;^   `=XQ@@k!`   `=hLD@@@@@@@b7^;_~o#|`  `~`          '`        
         'qQ~  `';BU,     ,?tQ@@@@@@@@Q%wjq@@%_    '*`        ~?:         
        .EQZt`   7m.     ~U8@@@@@@QXi~..+D@@Q~     ;L       ,JL`          
        \Q+.a<   .,,   `*Q@@@@@@b^`  'c8@@@@\      L^     `+&y`           
       .bS`'^y, ,xj`  ~X@@@@@@@Q; `!EQ@@@@@o`     `z_   .T&@m`            
       ~#! ,,!}^D@A``;X@@@@@QU@Q|iR@@@@@@@N,     `?R*``=%@@Q'             
       ^K. ,._KW@@Q^  J@@@@6_`jQQ@@@@@@@@@z      ~Q@%;{Q@@@J              
       ?r  _.Y8@@@@#; ;Q@@S.,zQ@@@@@@@@@@Q,      .SQyU@@@@U.              
       .  `;*Q@@@@@@QJ'z@B;zQ@@@@@@@@@@@@U`      '5@@@@@@Q;               
          ,6Q@@@@@@@@@8zkQQ@@@@#y?k@@@@@@o     `;5Q@@@@@@5`               
          !@@@@@@@QNgRQ@@@@@&y;.  +@@@@@@y    'iD@@@@@@@Q;                
          7@@@@NWQ6^|D@@@@P'.=+U* .D@@@@@m`  iQ@@@@@@@@@K`                
         `iK@@d'E@@@@@@@@@v7i;`.`  x@DD@@D..z@@@@@@@@@@@m                 
         .*!QQ~,K@@@@@@@@@z.'':cu}q#P'_%@Qzb@@@@@@@@@@@@U`                
         ,^`id;a@@@@@@@@@@D<ydQQDoyy=^y8@@@@@@@@@@@@@@@@Q:                
 ,c=.    ;;`;D@@@@@@@@@@@@QajJEEuaAw!^Q@@@@@@@@@@@@@@@@@@f`               
  `!t7~  +;y@@@@@@@@@@@@@@@Dq@@@@@@@@Q@@@@@@@@@@@@@@@@@@@Q*               
     ~zj|ysd@@@@Q@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Qz              
      `*Q@@@@@@RN@@@@@@@@@@@@@@@@qD@@QQQ8QQQ@@@@@@@@@@@@@@@@u'            
     >W@@@@@@@@N8@@@@Q@@QBQ@@@D%6:~bhkUknLs{yQ@@@@@@@QQQQQ@@@Q7'          
     J@@@@@@g8@@@@@@Q#@@NbXmE6Q@D~`nk|EamS}XWD@@Q=,^N=````':+Tokt;`       
     ,d@N~;y8BDW@@@8Q@@@@@Q@@@@@@Qn7RL;yU8Q@Q@@@I `~Wf.          '_`      
      'f%+` .^m@@@@Q@@@@@gqR8DQQQ@RjN%!7QNb%B@@@UaAb@Q8J,`                
        '+=!;~,*X@@@@@@@@wzUD5oP5fUQQQNRNQQ@QQNRQQ%Q%X6L|y;               
            `.''i&#@@@daz\7amyodd|i}RQQdRQNQQ@@@@@@QNQD:'r\5'             
                ygfQ@B7ci||*?*+*JUN@@@@@@@Q@@@QQ@@@@@@@Qo|7D_             
             .rYRQ7K@@qjx7v|?7mN@@@@@@@@@@QW@@@@@@@@@@@@@@@m`        `    
        `` `|&@@@@Qd@@@qyuDQQ@@@@@@@@@@@@@@@@@@@@@@@@@QQ@@@@%mL~`  rXjSf, 
    ;>.~wNJ=zD@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@NaiJdu?Qk`
   `5#DBQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ@@@@@@@@@@@@@@@@@@@@@@@@@QQQQQi

   YO HO, YO HO, A PIRATE'S LIFE FOR ME!"""

seed=False # if you want to seed the files you download, set it to True and make sure you have 'screen' installed
eztv_domain = 'eztv.re' # change it if domain is down, confirm working domain on: https://eztvstatus.com/
download_dir='/tmp/' # set your download dir
runtime=time.time()
search_results = []

def print_banner():
    for line in banner.splitlines():
        print(line.center(shutil.get_terminal_size().columns)); time.sleep(0.01)

def print_center(stdscr, text):
    stdscr.clear()
    h, w = stdscr.getmaxyx()
    x = w//2 - len(text)//2
    y = h//2
    stdscr.addstr(y, x, text)
    stdscr.refresh()

def download(magnet_link):
	print(f'Downloading in: {download_dir}')
	call(['aria2c', '--file-allocation=none', '--log-level=notice', '-l', f"{os.getcwd()}/{runtime}", f"--dir={download_dir}", '--download-result=full', '--seed-time=0', f"{magnet_link}"])
	if seed is True:
		print("We'll be good and seed in a screen session...")
		call(['screen', '-dm', 'aria2c', '-V', '--seed-ratio=0.0', f"--dir={download_dir}", f"{magnet_link}"])

def cleanup():
	log=f"{os.getcwd()}/{runtime}"
	if os.path.exists(log): os.remove(log) # cleanup logs

def get_subs(title):
    print('Searching for subs...')
    log=open(f"{os.getcwd()}/{runtime}",'r+')
    for line in log:
        if line.find('Download complete: /') >= 0:
            filename=line.split(' ')[6].rstrip()
    call(['addic7ed', '-r', 'sub', f"{filename}"])
    log.close
    cleanup()

def search_eztv(title):
    html_page = requests.get(f"https://{eztv_domain}/search/{title.replace(' ','-')}")
    soup = BeautifulSoup(html_page.content, 'html.parser')
    results=soup.find_all("a", class_='magnet')
    if title.lower() not in str(results[0]).lower():
        print("Show not found.")
        time.sleep(3)
        cleanup()
        quit()
    else:
        for show in results[:20]:
            search_results.append(f"{show.get('title'):<130s}         # ::::::::{requests.utils.unquote(show.get('href'))}")



def search_yts(title):
    data=requests.get(f"https://yts.mx/api/v2/list_movies.json?limit=10&query_term={title.replace(' ','+')}")
    parse=json.loads(data.content)
    if parse['data']['movie_count'] == 0:
        print("Movie not found.")
        time.sleep(3)
        cleanup()
        quit()
    else:
        all_results=parse['data']['movies']
        for torrent in all_results:
            search_results.append(f"{torrent['title']:<80s} # Seeds:{torrent['torrents'][0]['seeds']} # ::::::::magnet:?xt=urn:btih:{torrent['torrents'][0]['hash']}&dn={requests.utils.quote(torrent['title'])}&tr=udp://open.demonii.com:1337/announce&tr=udp://tracker.leechers-paradise.org:6969&tr=udp://tracker.openbittorrent.com:80&tr=udp://tracker.coppersurfer.tk:6969&tr=udp://glotorrents.pw:6969/announce&tr=udp://tracker.opentrackr.org:1337/announce&tr=udp://torrent.gresille.org:80/announce&tr=udp://p4p.arenabg.com:1337")


def main(stdscr):
    curses.curs_set(0)
    # color scheme for selected row
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
    trackers_title = "Jolly Roger cli torrent downloader v.2\n\n Select tracker:"
    trackers = ['EZTV Series','YTS Movies']
    tr_option,tr_index = pick(trackers,trackers_title)
    print_center(stdscr,"Enter title to  search: ")
    curses.echo()
    inp=stdscr.getstr()
    title=inp.decode()
    stdscr.refresh()
    if tr_index == 0:
        subs = ['Yes','No']
        subs_title = 'Should we look for subs?'
        subs_option,subs_index = pick(subs,subs_title)
        print_center(stdscr,"S E A R C H I N G . . .")
        search_eztv(title)
        res_title="Search results:"
        choice_option,choice_index = pick(search_results,res_title)
        os.system('clear')
        print(f"Starting download of: {choice_option.split('::::::::')[1]}")
        download(choice_option.split('::::::::')[1])
        if subs_index == 0:
            os.system('clear')
            get_subs(title)
        else:
            cleanup()
    elif tr_index == 1:
        print_center(stdscr,"S E A R C H I N G . . .")
        search_yts(title)
        res_title="Search results:"
        choice_option,choice_index = pick(search_results,res_title)
        os.system('clear')
        print(f"Starting download of: {choice_option.split('::::::::')[1]}")
        download(choice_option.split('::::::::')[1])
        cleanup()


### main block ###
try:
	null = open("/dev/null", "w")
	Popen("aria2c", stdout=null, stderr=null)
	null.close()
except OSError:
	print("This script depents on aria2 to download torrents, please install it: https://aria2.github.io/")
	quit()
try:
	null = open("/dev/null", "w")
	Popen("addic7ed", stdout=null, stderr=null)
	null.close()
except OSError:
	print("This script depents on addic7ed to download subtitles, please install it: sudo pip3 install addic7ed")
	quit()

try:
    print_banner()
    time.sleep(3)
    curses.wrapper(main)
except KeyboardInterrupt:
	print("\nwe got CTRL+C , exiting...")
	cleanup()
	quit()

# Wait and cleanup
#curses.napms(3000)
