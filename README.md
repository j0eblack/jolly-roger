## Overview

CLI torrent downloader, written in python3

The client is depending on several Python modules: BeautifulSoup, requests, addic7ed, curses, pick. Torrent download is handled by an awesome tool called aria2, download it from here and add it to your $PATH: https://aria2.github.io/

At the moment there are 2 trackers being searched for movies and tv shows: YTS & EZTV.

There is an option to download subtitles for TV shows using the addic7ed python module, none for movies for now.

The script can be easily extended to support more trackers, those are the ones that I was able to find with a good enough API.

Install dependencies: pip3 install -r requirements.txt

## v2 release notes:

* simplified search, you can use the following formats with EZTV: ex. 'bobs burgers' or 'bobs burgers s11e02'
* more user-friendly picking choice
* added option for seeding after download, works on linux only since it uses 'screen'

## Short FAQ:

* Why aren't you using RARBG? - They have an awesome API but the rate-limit is too harsh, not worth using it for now.

* Is this script safe to use? - Depends on your contry, check your local laws for torrenting.

* Will this script hide me from my government? - Not at all, check you local laws for torrenting.

* Can this run on Andoird? - Yes it can, you will need to change the pyhton interpreter path and make sure all of the modules are installed. Tested and working with Termux.

## License

```
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
```
